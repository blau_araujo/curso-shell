
# Aula 1: Revisão

## GitLab

https://gitlab.com/blau_araujo/curso-shell

## Bash

* Comando `echo`
* Criação e expansão de variáveis
* Parâmetros posicionais
* Comando `read`
* O comando composto `[[ EXPRESSÃO ]]`
* O comando composto `if`, `elif`, `else`
* O comando composto `case`
* Os comandos compostos `while` e `until`
* O comando composto `for`
* Funções
* Expansões condicionais
* Expansões de caixa de texto
* O redirecionamento de *append* `>>`

## Além do Bash

* O utilitário `chmod`
* O utilitário `mkdir`
* O utilitário `cat`
* O utilitário `setsid`

## Desafio!

