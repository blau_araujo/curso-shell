#!/usr/bin/env bash

#   Agrupamentos entre chaves
#
#   * Executado na sessão corrente
#
#   * Exige ';' no último comando
#
#   * Chaves são palavras reservadas
#     e exigem espaço

bicho=zebra

{
    echo "Dentro das chaves:"
    echo "Bicho: $bicho"

    fruta=banana
    bicho=gato

    echo "Fruta: $fruta"
    echo "Bicho: $bicho"
}

echo "Depois das chaves:"

echo "Bicho: $bicho"
echo "Fruta: $fruta"

