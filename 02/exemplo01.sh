#!/usr/bin/env bash

#   Agrupamentos entre parêntesis
#
#   * Cria um subshell
#
#   * Variáveis ciradas no subshell
#     são perdidas ao fim da execução
#
#   * Parêntesis são operadores e
#     não exigem espaço

bicho=zebra

(
    echo "Dentro dos parêntesis:"
    echo "Bicho: $bicho"

    fruta=banana
    bicho=gato

    echo "Fruta: $fruta"
    echo "Bicho: $bicho"
)

echo "Depois dos parêntesis:"

echo "Bicho: $bicho"
echo "Fruta: $fruta"

