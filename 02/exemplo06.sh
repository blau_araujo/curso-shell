#!/usr/bin/env bash

#   Funções são comandos compostos nomeados!
#
#   Comandos compostos:
#
#       * Agrupamentos de chaves
#       * Agrupamento de parêntesis
#       * Loop 'for'
#       * Loop 'while/until'
#       * Menu 'select'
#       * Bloco 'if'
#       * Bloco 'case'
#       * Teste com '[[ EXPRESSÃO ]]'
#       * Expressões aritméticas '(( EXPRESSÃO ))'
#
#   Sintaxe:
#
#       NOME() COMANDO_COMPOSTO
#       FUNCTION NOME COMANDO_COMPOSTO
#

# Criador de menus
PS3='Escolha uma opção: '

# Menu select com 'in "$@"' implícito...
menugen() select opt; do
    echo "($REPLY) $opt"
    [[ $REPLY -eq $# ]] && break
done

# Menu select "normal"...
menugen2() select opt in "$@" sair; do
    echo "($REPLY) $opt"
    [[ $REPLY -eq $(($# + 1)) ]] && break
done

menugen "$@" sair
echo
menugen2 "$@"
