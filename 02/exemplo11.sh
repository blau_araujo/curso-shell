#!/usr/bin/env bash

desenha_linha() {
    printf -v linha "%${2}s"
    echo ${linha// /$1}
}

desenha_linha $1 $2
