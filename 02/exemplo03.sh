#!/usr/bin/env bash

#   Funções são comandos compostos nomeados!
#
#   Comandos compostos:
#
#       * Agrupamentos de chaves
#       * Agrupamento de parêntesis
#       * Loop 'for'
#       * Loop 'while/until'
#       * Menu 'select'
#       * Bloco 'if'
#       * Bloco 'case'
#       * Teste com '[[ EXPRESSÃO ]]'
#       * Expressões aritméticas '(( EXPRESSÃO ))'
#
#   Sintaxe:
#
#       NOME() COMANDO_COMPOSTO
#       FUNCTION NOME COMANDO_COMPOSTO
#

# Variáveis globais por padrão / não abre subshell
eglobal() {
    echo 'Agrupamento com chaves: '
    ps ax | egrep '[0-9] bash ./exemplo03.sh'
}

# Variáveis locais por padrão / abre subshell
elocal() (
    echo 'Agrupamento com parêntesis: '
    ps ax | egrep '[0-9] bash ./exemplo03.sh'
)

echo
eglobal
echo
elocal
echo

exit
